import { Component, OnInit } from '@angular/core';
import { Football } from '../footballs';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orders: Array<Football> = [];
  constructor(private orderServce: OrderService) { }

  ngOnInit(): void {
    this.getALLArsenal();
  }

  getALLArsenal() {
    this.orderServce.getArsenal().subscribe(data => {
      this.orders = data;
    })
  }

}
