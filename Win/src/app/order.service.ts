import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Football } from './footballs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  getArsenal(): Observable<Array<Football>>{
    return this.http.get<Array<Football>>('assets/football.json');
  }
}
