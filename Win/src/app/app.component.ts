import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'workShop2';
  show:boolean = true;
  isShow:boolean = true;
  nickname = 'Winner';
  birthday = '20/12/2541';
  jobtitle = 'Developer';

  showProfile(){
    this.show = !this.show
  }

  showTable(){
    this.isShow = !this.isShow
  }
}
