import { Component, OnInit, Input } from '@angular/core';
import { Football } from '../footballs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() reviews: Array<Football> | undefined;
  options:string = '';
  constructor() { }

  ngOnInit(): void {
  }

  switchColor(param:string){
    this.options = param;
  }

}
