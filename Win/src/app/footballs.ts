export interface Football {
    id: string;
    name: string;
    national: string;
    position: string;
    number: string;
    price: string;
}
